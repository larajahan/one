
@extends('admin.admin_layouts')
@section('admin_content')

<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header w-75 offset-1">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Add Branch</span>
					</div>
				</div>
				<div class="panel_body w-75 offset-1">
					<div class="row">
						<div class="col-md-12 col-xs-12 ">
							
							<div class="card">
								<h5 class="card-header">Branch Form</h5>
								<div class="card-body">
									@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
									<form action="{{url('/admin/insert/branch')}}" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Branch Name</label>
													<input type="text" class="form-control "  name="name" value="">		
												</div>
											</div>
											<div class="col-md-4 ccol-xs-12">
												<div class="form-group">
													<label>Email</label>
													<input type="text" class="form-control " name="email" value="">	
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Phone</label>
													<input type="text" class="form-control "  name="phone" value="">	
												</div>
											</div>											
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Password</label>
													<input type="password" class="form-control "  name="password" value="">		
												</div>
											</div>
											<div class="col-md-4 ccol-xs-12">
												<div class="form-group">
													<label>Manager</label>
													<input type="text" class="form-control " name="manager" value="">	
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Manager Phone</label>
													<input type="text" class="form-control "  name="manager_phone" value="">	
												</div>
											</div>											
										</div>	
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>City</label>
													<input type="text" class="form-control "  name="city" value="">		
												</div>
											</div>
											<div class="col-md-4 ccol-xs-12">
												<div class="form-group">
													<label>Address</label>
													<input type="text" class="form-control " name="address" value="">	
												</div>
											</div>
											<div class="col-md-4 ccol-xs-12">
												<div class="form-group">
													<label>Zip Code</label>
													<input type="text" class="form-control " name="zipcode" value="">	
												</div>
											</div>
																					
										</div>									
										
										<button type="submit" class="btn btn-primary">Add Branch</button>
									</form>
								</div>
							</div>
							
							
						</div>
						
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
				</section>
				<!--/ page content -->
				<!-- start code here... -->
				</div><!--/middle content wrapper-->
				</div><!--/ content wrapper -->
				@endsection
@extends('admin.admin_layouts')
@section('admin_content')
<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			
			<div class="panel mb-0">
				<div class="panel_header w-75 offset-1">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Add Bank</span>
					</div>
				</div>
				<div class="panel_body w-75 offset-1">
					<div class="row">
						<div class="col-md-12 col-xs-12 ">
							<div class="card">
								<h5 class="card-header">Bank Holder Form</h5>
								<div class="card-body">
									<div class="modal-footer">
										<a type="submit" class="btn btn-primary" href="{{url('/admin/all/bankholder')}}">All Holders</a>
									</div>
									@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
									<form action="{{route('admin.insert.bankholder')}}" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Bank Name</label>
													<select class="custom-select" id="inputGroupSelect02" name="bank_id">
														<option selected>Select</option>
														@foreach($bankholders as $row)
														<option  value="{{$row->id}}">{{$row->bank_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4 ccol-xs-12">
												<div class="form-group">
													<label>Bank Holder</label>
													<input type="text" class="form-control " name="bank_holder_name" value="">
												</div>
											</div>
											<div class="col-md-4 ccol-xs-12">
												<div class="form-group">
													<label>Account Number</label>
													<input type="text" class="form-control " name="account_no" value="">
												</div>
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Address</label>
													<input type="text" class="form-control "  name="address" value="">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Known Person</label>
													<input type="text" class="form-control "  name="know_person" value="">
												</div>
											</div>
											<div class="col-md-4 ccol-xs-12">
												<div class="form-group">
													<label>Designation</label>
													<input type="text" class="form-control " name="designation" value="">
												</div>
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Mobile</label>
													<input type="text" class="form-control "  name="mobile" value="">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Opening Balance</label>
													<input type="number" class="form-control "  name="opening_blance" value="">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Balance</label>
													<input type="number" class="form-control "  name="balance" value="">
												</div>
											</div>
											
											
										</div>
										
										<button type="submit" class="btn btn-primary">Add Bank</button>
									</form>
								</div>
							</div>
							
							
						</div>
						
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
				</section>
				<!--/ page content -->
				<!-- start code here... -->
				</div><!--/middle content wrapper-->
				</div><!--/ content wrapper -->
				@endsection
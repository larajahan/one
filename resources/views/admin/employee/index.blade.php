@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->

      <!-- panel -->
         <div class="panel mb-0">


        <div class="panel_header">

          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Employee</span>
            <a href="{{route('admin.employee.create')}}" class="btn btn-success float-right">Add Employee</a>
          </div>
        </div>
        <div class="panel_body">

          <div class="table-responsive">
            <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
              <thead>
                <tr>
                  <th>Employee ID</th>
                  <th>Employee Name</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Mobile</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($employees as $employee)
                <tr>
                  <td >{{$employee->id}}</td>
                  <td>{{$employee->name}}</td>
                  <td>{{$employee->email}}</td>
                  <td>{{$employee->address}}</td>
                  <td>{{$employee->mobile}}</td>
                  <td><img id="logo" src="{{asset('public/panel/employee/'.$employee->image) }}" width="50" height="50;" /></td>                 
                  <td>
                    <!-- Example single danger button -->
                    <div class="btn-group">
                      <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('admin.employee.show',$employee->id)}}">View</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{route('admin.employee.edit',$employee->id)}}">edit</a>
                        <div class="dropdown-divider"></div>
                        <a id="delete-form-{{$employee->id}}" class="dropdown-item" href="">delete</a>

                      </div>
                    </div>
                    <form id="delete-form-{{$employee->id}}" action="{{route('admin.employee.destroy',$employee->id)}}" method="post" style="display: none">
                      @csrf
                      @method('DELETE')

                    </form>

                  </td>

                </tr>
                @endforeach
              </tbody>
            </table>

          </div>
          </div> <!--/ panel body -->
          </div><!--/ panel -->
        </section>
        <!--/ page content -->
        <!-- start code here... -->

        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->

        @endsection

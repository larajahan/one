@extends('admin.admin_layouts')
@section('admin_content')
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header ">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Loan From Bank</span>
					</div>
				</div>
				<div class="panel_body ">
					<div class="row">
						<div class="col-md-9 col-xs-12 ">
							
							<div class="card">
								<h5 class="card-header">Loan Form</h5>
								<div class="card-body">
									<div class="modal-footer">
										<span ><button type="submit" class="btn btn-primary " data-toggle="modal" data-target="#exampleModal">
											Add Loan From
										</button></span>
										<a type="submit" class="btn btn-primary" href="#">All Loan</a>
									</div>
									<!-- Modal -->
									<div class="modal fade mt-3" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog w-50" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Loan Added</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													@if ($errors->all())
													<div class="alert alert-danger">
														@foreach ($errors->all() as $error)
														<li>{{ $error }}</li>
														@endforeach
													</div>
													@endif
													<form action="{{route('admin.insert.loanfrom')}}" method="post">
														@csrf
														<div class="form-group">
															<label class="font-weight-bold" >Loan Name</label>
															<input type="text" class="form-control"  placeholder="Enter your Loan From" name="loanfrom" value="">
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														<button type="submit" class="btn btn-primary">Submit</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									{{-- Loan Form --}}
								@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
									<form action="{{route('admin.insert.loan')}}" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Date</label>
													<input type="date" class="form-control "  name="date" value="{{date('Y-m-d')}}"  >
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Loan From</label>
													<select class="form-control" id="loan_from" name="loan_from">
														<option  selected disabled="">Select</option>
														@foreach($loanfroms as $row)
														<option value="{{$row->id}}">{{$row->loanfrom}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Title</label>
													<input type="text" name="title"  class="form-control">
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Amount</label>
													<input type="text" class="form-control "  name="amount"  id="amount" onkeyup="sum();" >
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Interest</label>
													<input type="text" name="interest"  class="form-control" id="interest" onkeyup="sum();">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Payable</label>
													<input type="text" name="payable"  class="form-control" readonly=""  id="payable">
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Remarks</label>
													<input type="text" name="remarks"  class="form-control">
												</div>
											</div>
										</div>
										
										<button type="submit" class="btn btn-primary">Add Loan</button>
									</form>
								</div>
							</div>
							
							
						</div>
						<div class="col-md-3 col-xs-12">
							<div class="card">
								<h5 class="card-header">Loan From List</h5>
								<div class="card-body">
									<table class="table">
										<thead class="bg-primary text-light">
											<tr>
												<th >Name</th>
												<th >Action</th>
											</tr>
										</thead>
										<tbody>
											@foreach($loanfroms as $row)
											
											
											<tr>
												
												<td>{{$row->loanfrom}}</td>
												<td>
													<div class="dropdown">
														<button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														Action
														</button>
														<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															<a class="dropdown-item" href="{{route('admin.delete.loanfrom',$row->id)}}" id="delete"  >Delete</a>
															<a class="dropdown-item" href="{{route('admin.edit.loanfrom',$row->id)}}"  >Edit</a>
														</div>
													</div>
												</td>
												
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
				</section>
			</div>
		</div>
	
			
		<script type="text/javascript">
           function sum() {
               var amount = document.getElementById('amount').value;
               var interest = document.getElementById('interest').value;
                var result = parseInt(amount) - parseInt(interest);
               if (!isNaN(result)) {
               	 $('#payable').empty();
               document.getElementById('payable').value = parseInt(result);
               }
              }
             </script>
		
		
		
		
		@endsection
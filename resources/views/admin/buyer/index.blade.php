@extends('admin.admin_layouts')

@section('admin_content')
<!-- content wrpper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>


<div class="content_wrapper">
  <div class="middle_content_wrapper">
    <section class="page_content">
    <div class="panel mb-0">
      
    








<div class="inventory_ms mt-5">




    <form action="{{route('admin.buyer.store')}}" method="post">
      @csrf
      <div class="row">
        <div class="col-md-3 border py-2">
          <div class="form-group row">
            <div class="col-md-4">
              <span>Invoice No*</span>
            </div>
            <div class="col-md-8">
              <input class="form-control" type="text" required="" value="{{ mt_rand(9000,10000000) }}" name="invoice" readonly="">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              <span>Date</span>
            </div>
            <div class="col-md-8">
              <input class="form-control" type="date" name="date"  required="" value="{{date('Y-m-d')}}">
            </div>
          </div>
        </div>

        <div class="col-md-3 border py-2">
          <div class="form-group row">
            <div class="col-md-4">
              <span>Supplier ID*</span>
            </div>
            <div class="col-md-8">
              <input class="form-control" type="text"  name="supplierid"  required="" id="supplierid"  readonly="">
            </div>
          </div>
           <div class="form-group row">
            <div class="col-md-4">
              <span>Supplier Name</span>
            </div>
            <div class="col-md-8">
           <select  name="supplier_name" id="supplier_name" onchange="myFunction()">
             <option selected="" disabled="">==choose one==</option>
             @foreach (App\Supplier::orderBy('name', 'asc')->get() as $supp)
               <option value="{{ $supp->id }}">{{ $supp->name }}</option>
                @endforeach
            </select>
            </div>
          </div>
           </div>

        <div class="col-md-3 border py-2">
          <div class="form-group row">
            <div class="col-md-4">
              <span>Address</span>
            </div>
            <div class="col-md-8">
              <input class="form-control" type="text"  name="address"  required="" id="address" readonly="">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              <span>Mobile</span>
            </div>
            <div class="col-md-8">
              <input class="form-control" type="tel" name="mobile"  required="" id="mobile" readonly="">
            </div>
          </div>
        </div>

        <div class="col-md-3 border py-2">
          <div class="form-group row">
            <div class="col-md-4">
              <span>Category</span>
            </div>
            <div class="col-md-8">
             <select class="form-control" name="category_id">
                @foreach (App\Category::orderBy('name', 'asc')->get() as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
            </select>
            </div>
          </div>
          @php 
          $brand=DB::table('brands')->get();
          @endphp
          <div class="form-group row">
            <div class="col-md-4">
              <span>Company</span>
            </div>
            <div class="col-md-8">
              <select name="company" id="company" class="form-control"  name="company">
              @foreach($brand as $row)
                <option value="{{$row->id}}">{{$row->name}}</option>
              @endforeach
              </select>
            </div>
          </div>
        </div>
      </div>
               <div class="form-row">

                      <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Item Code</label>
                            <input type="text" class="form-control"  name="item_code" required="" id="product_code" readonly="">
                          </div>
                        </div>

                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                          <label>Item Name</label>
                          <select class="form-control" name="product_name" id="product_name" onchange="myFunction()">
                          <option selected="" disabled="">==choose one==</option>
                         @foreach (App\Product::orderBy('product_name', 'asc')->get() as $product)
                        <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                           @endforeach
                        </select>
                          </div>
                        </div>

                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Rate</label>
                            <input type="text" class="form-control"  name="rate"  id="rate"  readonly="" onkeyup="sum();">
                          </div>
                        </div>

                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>MRP</label>
                            <input type="text" class="form-control"  name="mrp" id="buy_price">
                          </div>
                        </div>

                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Unit</label>
                          <select class="form-control" name="unit_id">
                         @foreach (App\Unit::orderBy('name', 'asc')->get() as $unit)
                        <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                           @endforeach
                        </select>
                          </div>
                        </div>

                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                           <label>QTY</label>
                            <input type="number" class="form-control"  name="qty" id="qty" onkeyup="sum();">
                          </div>
                        </div>

                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Amount</label>
                            <input type="text" class="form-control"  name="amount" id="amount" readonly="" >
                          </div>
                        </div>

                      </div>
                       <button type="submit" class="btn btn-success float-left m-1">Add Buy </button>
                   </form>







      
       <div class="row mt-2">
        <div class="col-md-12">
          <table class="table table-bordered">
              <thead>
                <tr>     
                 <th>Invoice  No</th>
                  <th>Item Code</th>
                  <th>Item Name</th>
                  <th>Unit</th>
                  <th>QTY</th>
                  <th>Rate</th>
                  <th>Amount</th>    
                </tr>
              </thead>
              <tbody>
            

                @foreach($buyers as $buyer)
                <tr>
                 <td >{{$buyer->invoice}}</td>             
                 <td >{{$buyer->item_code}}</td>             
                 <td >{{$buyer->product_name }}</td>             
                 <td >{{$buyer->name }}</td>        
                 <td >{{$buyer->qty}}</td>                         
                 <td >{{$buyer->rate}}</td>                                
                 <td>{{ $buyer->amount}}</td>               
                </tr>
                @endforeach
              </tbody>       
            </table>
                  
          </div>
          </div>

          <div class="row justify-content-end">

            <div class="col-3 border py-1">
              <div class="row">
                <div class="col-4">
                  <span>Stok</span>
                </div>
                <div class="col-8">
                  <input type="number" value="1" class="form-control">
                </div>
              </div>
            </div><div class="col-3">
              <div class="row">
                <div class="col-12">
                  <button class="btn btn-success float-left m-1">Add</button>
                </div>
              </div>
            </div>
          </div>

          <div class="row mt-2">
            <div class="col-md-4">
              <div class="row form-group">
                <div class="col-5">
                  <span>Number of items:</span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control" placeholder="0">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-5">
                  <span>Total QTY:</span>
                </div>
                @php
                  $total_qty=App\Buyer::sum('qty');
                @endphp
                <div class="col-7">
                  <input type="text" class="form-control" placeholder="{{$total_qty}}"  >
                </div>
              </div>
              <div class="row form-group">
                <div class="col-5">
                  <span>Rmarks:</span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control" placeholder="0">
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="row form-group">
                <div class="col-4">
                  <span>Dis </span>
                </div>
                <div class="col-8">
                  <label>Dis (%)</label>
                  <input type="text" class="form-control" placeholder="0">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-4">
                  <span>Tax</span>
                </div>
                <div class="col-3">
                  <input type="text" class="form-control" placeholder="0">
                </div>
                <div class="col-5">
                  <input type="text" class="form-control" placeholder="0">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-4">
                  <span>Cash</span>
                </div>
                <div class="col-8">
                  <input type="text" class="form-control" placeholder="0">
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="row form-group">
                <div class="col-5">
                  <span>Total</span>
                </div>
                 @php
                  $total_amount=App\Buyer::sum('amount');
                @endphp
                <div class="col-7">
                  <input type="text" class="form-control" placeholder="{{$total_amount}}"  id="amount"  onkeyup="add();">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-5">
                  <span>Dis, Amount</span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control" id="dis" onkeyup="add();">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-5">
                  <span>Net Total</span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control" name="payable" id="payable">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-5">
                  <span>Due</span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control" placeholder="0">
                </div>
              </div>
            </div>
          </div>





          <!--/ row -->


          <!-- ============================== Short button ============================ -->
            <div class="justify-content-center row">
              <a href="#" class="btn btn-secondary m-1">F2-Print</a>
              <a href="#" class="btn btn-info m-1">F3-Show</a>
              <a href="#" class="btn btn-success m-1">F4-Save</a>
              <a href="#" class="btn btn-warning m-1">F5-New</a>
              <a href="#" id="delete" class="btn btn-danger m-1">Delete</a>
              <a href="#" class="btn btn-warning m-1">F7-View</a>
              <a href="#" class="btn btn-danger m-1">F8-Close</a>
            </div>

          </div>
      



      </div>
    </section>
  </div>
</div>




  
   <script type="text/javascript">
  
   $(document).ready(function() {
    $('select[name="supplier_name"]').on('change', function(){
    var supplierid = $(this).val();
    if(supplierid) {
    $.ajax({  
    url: "{{  url('/get/supplier/') }}/"+supplierid,
    type:"GET",
    dataType:"json",
    success:function(data) {
      
            $('#supplierid').empty();
                $('#supplierid').val(data.supplier_id);

                $('#mobile').empty();
                $('#mobile').val(data.mobile);

                $('#address').empty();
                $('#address').val(data.address);
                
        },
      });
    };
   });
   });    

 </script>

 <script type="text/javascript">
  
   $(document).ready(function() {
    $('select[name="product_name"]').on('change', function(){
    var productid = $(this).val();
    if(productid) {
    $.ajax({  
    url: "{{  url('/get/product/') }}/"+productid,
    type:"GET",
    dataType:"json",
    success:function(data) {
      
             $('#productid').empty();
                 $('#productid').val(data.product_code);
                  

                $('#product_code').empty();
                $('#product_code').val(data.product_code);
 
                $('#buy_price').empty();
                $('#buy_price').val(data.buy_price);

                 $('#rate').empty();
                $('#rate').val(data.buy_price);

                
        },
      });
    };
   });
   });    

 </script>
  <script type="text/javascript">
           function sum() {
               var qty = document.getElementById('qty').value;
               var rate = document.getElementById('rate').value;
                var result = parseInt(qty) * parseInt(rate) ;
               if (!isNaN(result)) {
                $('#amount').empty();
               document.getElementById('amount').value = parseInt(result);
               }
              }


             
 </script>

    <script type="text/javascript">
           function add() {
               var amount = document.getElementById('amount').value;
               var dis = document.getElementById('dis').value;
                var result = parseInt(amount) - parseInt(dis);
               if (!isNaN(result)) {
                 $('#payable').empty();
               document.getElementById('payable').value = parseInt(result);
               }
              }
             </script>
@endsection

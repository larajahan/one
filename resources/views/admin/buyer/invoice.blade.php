<div class="panel mb-0">						
	 <div class="row">
          <div class="col-sm-6 item_right text-right">
              <h3 class="mt-0">Invoice {{$buyer->product_code}}</h3>
              <div class="fs-13">Date {{$buyer->date}} </div>
                <div class="text-danger fs-13">Buyer ID: {{$buyer->supplierid}} </div>
              <div class="text-danger fs-13">Buyer Name: </div>
              <address class="fs-13 mt-3">
                  <strong>Address.</strong><br>
                  {{$buyer->address}}<br>
                 Mobile Number<br>
                  <abbr title="Phone">Ban:</abbr>{{$buyer->mobile}}
              </address>
          </div>
      </div>
      <hr>
      <div class="table-responsive mb-3">
          <table class="table table-striped">
              <thead>
                  <tr>
                      <th class="border-top-0">Item Name</th>
                      <th class="border-top-0">Quantity</th>
                      <th class="border-top-0">Unit Price</th>
                      <th class="border-top-0">Tax</th>
                      <th class="border-top-0">Total Price</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td><div><strong>{{$buyer->name }}</strong></div>
                        </td>
                      <td>{{$buyer->qty}}</td>
                      <td>{{$buyer->rate}}</td>
                      <td>{{$buyer->tax}}</td>
                      <td>{{$buyer->amount}}</td>
                  </tr> 
              </tbody>
          </table>
      </div>
      <div class="row">
        <div class="col-sm-6">
        	<ul class="list-unstyled">
              <li>
                  <strong>Sub - Total amount:</strong> $9265 </li>
              <li>
                  <strong>Discount:</strong> 12.9% </li>
              <li>
                  <strong>VAT:</strong> ----- </li>
              <li>
                  <strong>Grand Total:</strong> $12489 </li>
          </ul>
        </div>
      
      </div>
</div>


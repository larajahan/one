@extends('admin.admin_layouts')

@section('admin_content')
<!-- content wrpper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Edit Product Buy</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="row">
            <div class="col-md-10 col-xs-12 offset-1">
              <div >
                <div class="card">
                  <h5 class="card-header">Product by  Form
             <!--          <span style="float: right;"> <input type="checkbox" id="havecode" onchange="myFunction()"> I have a buy code </span> -->
                  </h5>
                  <div class="card-body">
                    @if ($errors->all())
                    <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </div>
                    @endif

                    <form action="{{route('admin.buyer.update',$buyer->id)}}" enctype="multipart/form-data" method="post">
                      @csrf
                      <div class="form-row">
                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Invoice No</label>
                            <input type="text" class="form-control"  name="product_code"  required="" value="{{ mt_rand(9000,10000000) }}" readonly="">
  
                          </div>
                        </div>

                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Date</label>
                            <input type="date" class="form-control"  name="date"  required="" value="{{date('Y-m-d')}}">
                          </div>
                        </div>
               
                           <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Supplier</label>
                          <select class="form-control" name="supplier_name" id="supplier_name" onchange="myFunction()">
                         <option selected="" disabled="">==choose one==</option>
                         @foreach (App\Supplier::orderBy('name', 'asc')->get() as $supp)
                        <option value="{{ $supp->id }}">{{ $supp->name }}</option>
                           @endforeach
                        </select>
                          </div>
                        </div>

                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Supplier ID</label>
                            <input type="text" class="form-control"  name="supplierid"  required="" id="supplierid"  readonly="">
                              
                          </div>
                        </div>

                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control"  name="address"  required="" id="address" readonly="">
                          </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Mobile  </label>
                            <input type="text" class="form-control"  name="mobile"  required="" id="mobile" readonly="">
                          </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Category</label>
                          <select class="form-control" name="category_id">
                         @foreach (App\Category::orderBy('name', 'asc')->get() as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                           @endforeach
                        </select>
                          </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Company  </label>
                            <input type="text" class="form-control"  name="company" value="{{$buyer->company}}">
                          </div>
                        </div>
                           
                      </div>
                   <div class="form-row">
                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Item Code</label>
                            <input type="text" class="form-control"  name="item_code" required="" id="product_code" readonly="">
                          </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                          <label>Item Name</label>
                          <select class="form-control" name="product_name" id="product_name" onchange="myFunction()">
                          <option selected="" disabled="">==choose one==</option>
                         @foreach (App\Product::orderBy('product_name', 'asc')->get() as $product)
                        <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                           @endforeach
                        </select>
                          </div>
                        </div>
                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Rate</label>
                            <input type="text" class="form-control"  name="rate"  id="rate"  readonly="" nkeyup="sum();">
                          </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>MRP</label>
                            <input type="text" class="form-control"  name="mrp" id="buy_price">
                          </div>
                        </div>
                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Unit</label>
                          <select class="form-control" name="unit_id">
                         @foreach (App\Unit::orderBy('name', 'asc')->get() as $unit)
                        <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                           @endforeach
                        </select>
                          </div>
                        </div>

                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>QTY</label>
                            <input type="number" class="form-control"  name="qty" id="qty" onkeyup="sum();">
                          </div>
                        </div>

                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Amount</label>
                            <input type="text" class="form-control"  name="amount" id="amount" readonly="" >
                          </div>
                        </div>

                           <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>Disc</label>
                            <input type="text" class="form-control"  name="disc" value="
                            0">
                          </div>
                        </div>

                         <div class="col-md-3 col-xs-12">
                          <div class="form-group">
                            <label>tax</label>
                            <input type="text" class="form-control"  name="tax" value="0">
                          </div>
                        </div>
                        
                      </div>
                      <div class="row">   
                      </div> 
                      <button type="submit" class="btn btn-primary">Update Buy </button>
                    </form>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          
        </div>
        </div> <!--/ panel body -->
        </div><!--/ panel -->
      </section>
    </div><!--/middle content wrapper-->
  </div><!--/ content wrapper -->
 <script type="text/javascript">
 	
 	 $(document).ready(function() {
		$('select[name="supplier_name"]').on('change', function(){
		var supplierid = $(this).val();
		if(supplierid) {
		$.ajax({	
		url: "{{  url('/get/supplier/') }}/"+supplierid,
		type:"GET",
		dataType:"json",
		success:function(data) {
		  
		        $('#supplierid').empty();
                $('#supplierid').val(data.supplier_id);

                $('#mobile').empty();
                $('#mobile').val(data.mobile);

                $('#address').empty();
                $('#address').val(data.address);
                
		    },
		  });
		};
	 });
   });	 	

 </script>

 <script type="text/javascript">
 	
 	 $(document).ready(function() {
		$('select[name="product_name"]').on('change', function(){
		var productid = $(this).val();
		if(productid) {
		$.ajax({	
		url: "{{  url('/get/product/') }}/"+productid,
		type:"GET",
		dataType:"json",
		success:function(data) {
		  
		         $('#productid').empty();
                 $('#productid').val(data.product_code);
                  

                $('#product_code').empty();
                $('#product_code').val(data.product_code);
 
                $('#buy_price').empty();
                $('#buy_price').val(data.buy_price);

                 $('#rate').empty();
                $('#rate').val(data.buy_price);

                
		    },
		  });
		};
	 });
   });	 	

 </script>
 	<script type="text/javascript">
           function sum() {
               var qty = document.getElementById('qty').value;
               var rate = document.getElementById('rate').value;
                var result = parseInt(qty) * parseInt(rate) ;
               if (!isNaN(result)) {
               	$('#amount').empty();
               document.getElementById('amount').value = parseInt(result);
               }
              }
             </script>
@endsection

@extends('admin.admin_layouts')
@section('admin_content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header w-75 offset-1">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Withdraw From Bank</span>
					</div>
				</div>
				<div class="panel_body w-75 offset-1">
					<div class="row">
						<div class="col-md-12 col-xs-12 ">
							
							<div class="card">
								<h5 class="card-header">Withdraw Form</h5>
								<div class="card-body">
									<div class="modal-footer">
										<a type="submit" class="btn btn-primary" href="{{url('/admin/all/withdraw')}}">All Withdraw</a>
									</div>
									@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
									<form action="{{route('admin.insert.withdraw')}}" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Invoice No</label>
													<input type="text" class="form-control "  name="invoice_no"
													value="{{ mt_rand(9000,10000000) }}" readonly>
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Date</label>
													<input type="date" class="form-control "  name="date" value="{{date('Y-m-d')}}"  >
												</div>
											</div>
											
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Bank Name</label>
													<select class="form-control dynamic"  name="bank" id="bank"  >
														<option selected disabled="">Select</option>
														@foreach($banks as $bank)
														<option value="{{$bank->id}}">{{$bank->bank_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											
											
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Account Holder</label>
													<select class="form-control " name="account_holder_name"  id="account_holder_name"  >
													</select>
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Account Number</label>
													<input type="text" name="account_no" readonly="" id="account_no" class="form-control">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Pay Mode</label>
													<select class="form-control" id="name" name="pay_mode">
														<option  selected disabled="">Select</option>
														<option value="bank" name="bank">Bank</option>
														<option value="cash">Cash</option>
														<option value="bonus">Bonus</option>
													</select>
												</div>
											</div>
											
										</div>
										
										<div class="form-row bank box">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<span style="float: right; border: 1px solid grey; padding: 2px"> Balance: <span id="senderbalance" > </span> </span>
													<label> Send Bank </label>
													<select class="form-control"  name="recive_bank" id="recive_bank">
														<option selected disabled="">Select</option>
														@foreach($banks as $bank)
														<option value="{{$bank->id}}">{{$bank->bank_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Account Holder</label>
													<select class="form-control " name="recive_holder_name"  id="recive_holder_name"  >
														
													</select>
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Check No / Transaction</label>
													<input type="text" class="form-control "  name="check_no" value=""  >
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Payment Amount</label>
													<input type="text" name="payment_amount"   class="form-control">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Remarks</label>
													<input type="text" name="remarks"  class="form-control">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												
											</div>
											
											
											
										</div>
										
										
										
										<span style="float: right; border: 1px solid grey; padding: 10px">Instant Balance: <span id="instant" > </span> </span>
										<button type="submit" class="btn btn-primary">Add withdraw from Bank</button>
									</form>
								</div>
							</div>
							
							
						</div>
						
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
				</section>
			</div>
		</div>
		<script>
			
		$(document).ready(function(){
		$("#name").change(function(){
		$(this).find("option:selected").each(function(){
		var optionValue = $(this).attr("value");
		if(optionValue){
		$(".box").not("." + optionValue).hide();
		$("." + optionValue).show();
		} else{
		$(".box").hide();
		}
		});
		}).change();
		});
		</script>
		<script type="text/javascript">
		
		$(document).ready(function() {
		$('select[name="bank"]').on('change', function(){
		var bankid = $(this).val();
		if(bankid) {
			$.ajax({
		url: "{{  url('/get/holder/') }}/"+bankid,
		type:"GET",
		dataType:"json",
		success:function(data) {
		
		$('#account_holder_name').empty();
		$('#account_holder_name').append(' <option value="">--Select--</option>');
		$.each(data,function(index,data){
		$('#account_holder_name').append('<option value="' + data.id + '">'+data.bank_holder_name+'</option>');
		});
		},
		
		});
		} else {
		alert('danger');
		}
		});
		// subdistrict
		$('select[name="account_holder_name"]').on('change', function(){
		var hold_id = $(this).val();
		if(hold_id) {
		$.ajax({
		url: "{{  url('/get/details/holder/') }}/"+hold_id,
		type:"GET",
		dataType:"json",
		success:function(data) {
		
		// var d =$('.rc').empty();
				$('#account_no').empty();
		$('#instant').empty();
		// $('.rc').append( data.name);
		// $('#rate').append( data.sell_rate);
				$('#account_no').val( data.account_no);
		$('#instant').append( data.balance);
		}
		});
		} else {
		alert('danger');
		}
		});
		//fgfdg
		$('select[name="recive_bank"]').on('change', function(){
		var paybank_id = $(this).val();
		if(paybank_id) {
			$.ajax({
		url: "{{  url('/get/pay/bank') }}/"+paybank_id,
		type:"GET",
		dataType:"json",
		success:function(data) {
			
				$('#recive_holder_name').empty();
		$('#recive_holder_name').append(' <option value="">--Select--</option>');
		$.each(data,function(index,data){
		$('#recive_holder_name').append('<option value="' + data.id + '">'+data.bank_holder_name+'</option>');
		});
		},
		
		});
		} else {
		alert('danger');
		}
		});
		$('select[name="recive_holder_name"]').on('change', function(){
		var payment_id = $(this).val();
		if(payment_id) {
		$.ajax({
		url: "{{  url('/get/payment/bank/') }}/"+payment_id,
		type:"GET",
		dataType:"json",
		success:function(data) {
		
		$('#senderbalance').empty();
		$('#senderbalance').append( data.balance);
		}
		});
		} else {
		alert('danger');
		}
		});
		});
		
		
		</script>
		
		@endsection
@extends('admin.admin_layouts')

@section('admin_content')

<div class="content_wrapper">
  <div class="middle_content_wrapper">
    <section class="page_content">
         <div class="panel mb-0">
         <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Product</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="table-responsive">
            <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
              <thead>
                <tr>     
                  <th>Product Code</th>
                  <th>Product Name</th>
                  <th>Buy Price</th>
                  <th>Sell Price</th>
                  <th>Stock</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($products as $product)
                <tr>
            
                 <td >{{$product->product_code}}</td>             
                  <td >{{$product->product_name}}</td>                             
                  <td >{{$product->buy_price}}</td>   
                  <td> {{$product->sell_price}}</td> 
                    <td >{{$product->stock}}</td> 
                  <td><img id="logo" src="{{asset('public/panel/product/'.$product->image) }}" width="50" height="50;" /></td> 
                  <td>
                    @if($product->status == 1)
                    <span class="badge badge-success">Active</span>
                    @else
                    <span class="badge badge-danger">Inactive</span>
                    @endif
                  </td>          
                    
                  <td>

                 <div class="dropdown">
              <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Action
              </button>
               <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="{{ route('admin.product.edit', $product->id) }}">Edit</a>
                  <a class="dropdown-item" href="{{ route('admin.product.barcode', $product->id) }}">Barccode</a>
                  <a class="dropdown-item" href="{{ route('admin.product.view', $product->id) }}">View</a>
                  <a class="dropdown-item" href="{{ route('admin.product.delete', $product->id) }}" id="delete">Delete</a>
      
             </div>
             </div>

                   <!--  <div class="btn-group" role="group">
                     <a href="{{ route('admin.product.view', $product->id) }}"   class="btn btn-sm btn-blue">view</a>
                      <a href="{{ route('admin.product.delete', $product->id) }}" id="delete"  class="btn btn-sm btn-danger">Delete</a>
                      <a href="{{ route('admin.product.edit', $product->id) }}" class="btn btn-sm btn-primary">Edit</a>
                    </div> -->
                  </td>

                </tr>
                @endforeach
              </tbody>
            </table>

          </div>
          </div> <!--/ panel body -->
          </div><!--/ panel -->
        </section>
        <!--/ page content -->
        <!-- start code here... -->

        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->

        @endsection

       
@extends('admin.admin_layouts')

@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->
      
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Website Setting</span>
          </div>
        </div>
        <div class="panel_body">
          
          <div class="row">
            <div class="col-md-10 col-xs-12 offset-1">
              <div >
                <div class="card">
                  <h5 class="card-header">Edit Setting</h5>
                  <div class="card-body">                   
                        <form action="{{ url('/admin/setting/update') }}" enctype="multipart/form-data" method="post">
                          @csrf
                          <div class="form-row">
                            <div class="col-md-3 col-xs-12">
                              <div class="form-group">
                                <label for="sname">Working Day</label>
                                <input type="text" class="form-control" name="working_day" value="{{$settings->working_day}}" >
                              </div>
                            </div>

                             <div class="col-md-3 col-xs-12">
                              <div class="form-group">
                                <label for="sname">Vat</label>
                                <input type="text" class="form-control"  name="vat" value="{{$settings->vat}}">
                              </div>
                            </div>
                            <div class="col-md-3 ccol-xs-12">
                              <div class="form-group">
                                <label> Email</label>
                                <input type="text" class="form-control" name="email" value="{{$settings->email}}">
                              </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                              <div class="form-group">
                                <label> Phone One</label>
                                <input type="text" class="form-control"  name="phone_one" value="{{$settings->phone_one}}">
                              </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                              <div class="form-group">
                                <label> Phone Two</label>
                                <input type="text" class="form-control"  name="phone_two" value="{{$settings->phone_two}}">
                              </div>
                            </div>
                               <div class="col-md-3 col-xs-12">
                              <div class="form-group">
                                <label> Company Name</label>
                                <input type="text" class="form-control"  name="company_name" value="{{$settings->company_name}}">
                              </div>
                            </div>
                           
                          </div>
                          <div class="form-row">
                            <div class="col-md-3 col-xs-12">
                              <div class="form-group">
                                <label> Address</label>
                                <input type="text" class="form-control"  name="address" value="{{$settings->address}}">
                              </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                              <div class="form-group">
                                <label>Zip</label>
                                <input type="text" class="form-control"  name="zipcode" value="{{$settings->zipcode}}">
                              </div>
                            </div>
                        
                          </div>

                          <div class="form-row">               
                              <div class="col-md-4 col-xs-12">
                               <div class="form-group">
                                <label>Upload Logo</label> 
                                 <img src="" id="show">                     
                               <input type="file"   name="image" id="cust_image" onchange="readURL(this);">
                           </div>                           
                          <input type="hidden" name="oldpic" value="{{$settings->image }}">
                          <input type="hidden" name="id" value="{{$settings->id }}">
                        </div>             
                       <div class="col-md-4 col-xs-12">
                       <div class="form-group">
                      <img src="{{ URL::to('public/panel/setting/'.$settings->image) }}" width="100" height="100" />                   
                      </div>               
                  </div>

                   <div class="col-md-4 col-xs-12">
               </div>
             </div>
                          <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                      
                    
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          
          </div> <!--/ panel body -->
          </div><!--/ panel -->
        </section>
        <!--/ page content -->
        <!-- start code here... -->
        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->
        <script type="text/javascript">
            function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) 
                {
                $('#logo')
                  .attr('src', e.target.result)
                  .width(80)
                  .height(80);
                       };
            reader.readAsDataURL(input.files[0]);
            }
        }
        </script>

        @endsection
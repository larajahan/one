@extends('admin.admin_layouts')

@section('admin_content')
<section class="invoice_area">
					<div class="panel mb-0">
						<div class="panel_header">
							<div class="panel_title">
								<span>Invoice</span>
							</div>
						</div>
						<div class="panel_body print_element">

                          	<div class="logo bg-blue">
                              <img src="assets/images/logo.png" class="img-fluid" alt="">
                          	</div>
							 <div class="row">
	                              <div class="col-sm-6">
	                                  <br>
	                                  <address class="fs-13">
	                                      <strong>Twitter, Inc.</strong><br>
	                                      1355 Market Street, Suite 900<br>
	                                      San Francisco, CA 94103<br>
	                                      <abbr title="Phone">P:</abbr> (123) 456-7890
	                                  </address>
	                                  <address class="fs-13">
	                                      <strong>Full Name</strong><br>
	                                      <a href="mailto:#">first.last@example.com</a>
	                                  </address>
	                              </div>
	                              <div class="col-sm-6 item_right text-right">
	                                  <h3 class="mt-0">Invoice #0044777</h3>
	                                  <div class="fs-13">Issued March 19th, 2019</div>
	                                  <div class="text-danger fs-13">Payment due April 21th, 2019</div>
	                                  <address class="fs-13 mt-3">
	                                      <strong>Twitter, Inc.</strong><br>
	                                      1355 Market Street, Suite 900<br>
	                                      San Francisco, CA 94103<br>
	                                      <abbr title="Phone">P:</abbr> (123) 456-7890
	                                  </address>
	                              </div>
	                          </div>
	                          <hr>
	                          <div class="table-responsive mb-3">
	                              <table class="table table-striped">
	                                  <thead>
	                                      <tr>
	                                          <th class="border-top-0">Item List</th>
	                                          <th class="border-top-0">Quantity</th>
	                                          <th class="border-top-0">Unit Price</th>
	                                          <th class="border-top-0">Tax</th>
	                                          <th class="border-top-0">Total Price</th>
	                                      </tr>
	                                  </thead>
	                                  <tbody>
	                                      <tr>
	                                          <td><div><strong>Lorem Ipsum is simply dummy text</strong></div>
	                                              <small>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</small></td>
	                                          <td>1</td>
	                                          <td>$39.00</td>
	                                          <td>$71.98</td>
	                                          <td>$27,98</td>
	                                      </tr>
	                                      <tr>
	                                          <td>
	                                              <div><strong>It is a long established fact that a reader will be</strong></div>
	                                              <small>There are many variations of passages of Lorem Ipsum available, but the majority</small>
	                                          </td>
	                                          <td>2</td>
	                                          <td>$57.00</td>
	                                          <td>$56.80</td>
	                                          <td>$112.80</td>
	                                      </tr>
	                                      <tr>
	                                          <td><div><strong>The standard chunk of Lorem Ipsum used since</strong></div>
	                                              <small>It has survived not only five centuries, but also the leap into electronic .</small></td>
	                                          <td>3</td>
	                                          <td>$645.00</td>
	                                          <td>$321.20</td>
	                                          <td>$1286.20</td>
	                                      </tr>
	                                      <tr>
	                                          <td><div><strong>The standard chunk of Lorem Ipsum used since</strong></div>
	                                              <small>It has survived not only five centuries, but also the leap into electronic .</small></td>
	                                          <td>3</td>
	                                          <td>$486.00</td>
	                                          <td>$524.20</td>
	                                          <td>$789.20</td>
	                                      </tr>
	                                  </tbody>
	                              </table>
	                          </div>
                              <div class="row">
                                <div class="col-sm-6">
                                	<ul class="list-unstyled">
                                      <li>
                                          <strong>Sub - Total amount:</strong> $9265 </li>
                                      <li>
                                          <strong>Discount:</strong> 12.9% </li>
                                      <li>
                                          <strong>VAT:</strong> ----- </li>
                                      <li>
                                          <strong>Grand Total:</strong> $12489 </li>
                                  </ul>
                                </div>
                                <div class="col-sm-6 item_right paypal">
                                  <img src="assets/images/paypal.png" class="img-fluid" alt="">
                                </div>
                              </div>
						</div>
						<div class="panel_footer text-right">
							<button type="button" class="btn btn-info print"><span class="fa fa-print"></span></button>
							<button type="button" class="btn btn-blue"><i class="fas fa-dollar-sign"></i> Make A Payment</button>
						</div>
					</div>
				</section>

 @endsection
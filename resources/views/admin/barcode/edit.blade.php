@extends('admin.admin_layouts')

@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Edit Category</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="row">
            <div class="col-md-10 col-xs-12 offset-1">
              <div >
                <div class="card">
                  <h5 class="card-header">Edit Category</h5>
                  <div class="card-body">
                    @if ($errors->all())
                    <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </div>
                    @endif
                    <form action="{{route('admin.category.update',$category->id)}}" enctype="multipart/form-data" method="post">
                      @csrf
                      <div class="form-row">
                        <div class="col-md-6 col-xs-12">
                          <div class="form-group">
                            <label>Employee Name</label>
                            <input type="text" class="form-control"  name="name" value="{{$category->name}}" >
                          </div>
                        </div>
                    </div>
                 </div>
                      <button type="submit" class="btn btn-primary">Update Category</button>
                    </form>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          
        </div>
        </div> <!--/ panel body -->
        </div><!--/ panel -->
      </section>
      <!--/ page content -->
      <!-- start code here... -->
      </div><!--/middle content wrapper-->
      </div><!--/ content wrapper -->
     
       

@endsection
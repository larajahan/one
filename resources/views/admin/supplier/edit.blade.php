@extends('admin.admin_layouts')
@section('admin_content')
<div class="container mt-3">
  <div class="row">
    <div class="col-md-12 ">
      <div class="card">
        <div class="card-header bg-primary text-white">
          Update Supplier
        </div>
        <div class="card-body">
          @if ($errors->all())
          <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </div>
          @endif
          <form action="{{url('/admin/edit/supplier/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label >Supplier Name</label>
                  <input type="hidden" class="form-control"  name="id" value="{{ $suplieredit->id}}" >
                  <input type="text" class="form-control "  name="name" value="{{ $suplieredit->name}}" >
                </div>
              </div>
              <div class="col-md-3 ccol-xs-12">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" name="email" value="{{ $suplieredit->email}}">
                </div>
              </div>
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Address</label>
                  <input type="text" class="form-control"  name="address" value="{{ $suplieredit->address}}">
                </div>
              </div>
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Mobile</label>
                  <input type="text" class="form-control"  name="mobile" value="{{ $suplieredit->mobile}}">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Position</label>
                  <input type="text" class="form-control"  name="position" value="{{ $suplieredit->position}}">
                </div>
              </div>
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Fax</label>
                  <input type="text" class="form-control"  name="fax" value="{{ $suplieredit->fax}}">
                </div>
              </div>
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Contact Person</label>
                  <input type="text" class="form-control"  name="contact_person" value="{{ $suplieredit->contact_person}}">
                </div>
              </div>
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Mobile Company</label>
                  <input type="text" class="form-control"  name="mobile_company" value="{{ $suplieredit->mobile_company}}">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Bank Name</label>
                  <input type="text" class="form-control"  name="bank_name" value="{{ $suplieredit->bank_name}}">
                </div>
              </div>
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Account Name</label>
                  <input type="text" class="form-control"  name="account_name" value="{{ $suplieredit->account_name}}">
                </div>
              </div>
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Account NO</label>
                  <input type="text" class="form-control
                  "  name="account_number" value="{{ $suplieredit->account_number}}">
                </div>
              </div>
              <div class="col-md-3 col-xs-12">
                <div class="form-group">
                  <label>Brance Name</label>
                  <input type="text" class="form-control"  name="branch" value="{{ $suplieredit->branch}}">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label>Opening Balance</label>
                  <input type="text" class="form-control"  name="opening_balance" value="{{ $suplieredit->opening_balance}}">
                </div>
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label>Upload Logo</label>
                  <img src="" id="show">
                  <input type="file"   name="image"id="image" onchange="readURL(this);">
                </div>
                <input type="hidden" name="oldpic" value="{{$suplieredit->image }}">
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <img src="{{ URL::to('public/panel/assets/images/supplier/'.$suplieredit->image) }}" width="100" height="100" />
                  
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script >
function readURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function (e)
{
$('#show')
.attr('src', e.target.result)
.width(80)
.height(80);
};
reader.readAsDataURL(input.files[0]);
}
}
</script>
@endsection
@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->


      <div class="row"> 
        <div class="col-md-8 offset-1"> 
      <div class="panel mb-0">
        <div class="panel_header ">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Supplier List</span>
          </div>
        </div>
        <div class="panel_body  ">
          
          
          <div class="row">
            <div class="col-md-4 ">
              <div class="profile">
                {{-- <img src="https://www.gravatar.com/avatar/e4d6f769d84602ac115aa4c29797ba25?s=250" alt=""  class="img-fluid rounded-circle"> --}}
                <img id="logo" src="{{asset('public/panel/assets/images/supplier/'.$single_view->image) }}" alt=""  class="img-fluid rounded-circle" />
              </div>
            </div>
            <div class="col-md-8">
              <div class="user_info">
                <div class="table-responsive">
                  <table class="table table-hover mt-2">                  
                    <tbody>
                      <tr>
                        <td class="font-weight-bold">Supplier ID:</td>
                         <td>{{$single_view->supplier_id}}</td>
                      </tr>
                      <tr>
                        <tr>
                        <td class="font-weight-bold">Supplier Name:</td>
                         <td>{{$single_view->name}}</td>
                      </tr>
                      <tr>
                        <td class="font-weight-bold">Email:</td>
                        <td>{{$single_view->email}}</td>
                      </tr>
                      <tr>
                        <td class="font-weight-bold">Address:</td>
                        <td>{{$single_view->address}}</td>
                      </tr> 
                      <tr>
                        <td class="font-weight-bold">Mobile:</td>
                        <td>{{$single_view->mobile}}</td>
                      </tr> 
                      <tr>
                        <td class="font-weight-bold">Position:</td>
                        <td>{{$single_view->position}}</td>
                      </tr> 
                      <tr>
                        <td class="font-weight-bold">Fax:</td>
                        <td>{{$single_view->fax}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Contact Person:</td>
                        <td>{{$single_view->contact_person}}</td>
                      </tr> 
                      <tr>
                        <td class="font-weight-bold">Mobile Company:</td>
                        <td>{{$single_view->mobile_company}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Bank Name:</td>
                        <td>{{$single_view->bank_name}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Account Name:</td>
                        <td>{{$single_view->account_name}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Account NO:</td>
                        <td>{{$single_view->account_number}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Brance Name:</td>
                        <td>{{$single_view->branch}}</td>
                      </tr> 
                      <tr>
                        <td class="font-weight-bold">Opening Blance:</td>
                        <td>{{$single_view->opening_balance}}</td>
                      </tr>
                        <tr>
                        
                         <td><a class="btn btn-info btn-sm" href="{{url('/admin/list/supplier')}}">Back</a></td>

                      </tr>
                    </tbody>                  
                  </table>               
            </div>         
          </div>
          
          
          
         
          
          </div> <!--/ panel body -->
          </div><!--/ panel -->
          </div>
           </div>
        </section>
        <!--/ page content -->
        <!-- start code here... -->
        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->
        @endsection
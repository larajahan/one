@extends('admin.admin_layouts')
@section('admin_content')
@php  
 $banks=DB::table('banks')->get();
 $bankholder=DB::table('bankholders')->get();

@endphp

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header w-75 offset-1">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Edit Deposit to Bank</span>
					</div>
				</div>
				<div class="panel_body w-75 offset-1">
					<div class="row">
						<div class="col-md-12 col-xs-12 ">
							
							<div class="card">
								<h5 class="card-header">Deposit Form</h5>
								<div class="card-body">
									<div class="modal-footer">
										<a type="submit" class="btn btn-primary" href="{{url('/admin/all/deposits')}}">All Deposits</a>
									</div>
									@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
									<form action="{{route('admin.update.deposits')}}" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<input type="hidden" class="form-control"  name="id" value="{{ $depositedit->id}}" >
													<label>Invoice Name</label>
													<input type="text" class="form-control "  name="invoice_no"
													value="{{ $depositedit->invoice_no}}" readonly>
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Date</label>
													<input type="date" class="form-control "  name="date" value="{{date('Y-m-d')}}"  >
												</div>
											</div>
											
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Bank Name</label>
													<select class="form-control dynamic"  name="sender_bank" id="sender_bank"  >
														<option selected disabled="">Select</option>
														@foreach($banks as $bank)
														<option value="{{$bank->id}}"

															<?php if ($bank->id == $depositedit->sender_bank) {
															echo "selected";
														} ?>>{{$bank->bank_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">

													<label>Account Holder</label>
													<select class="form-control " name="sender_account_holder"   id="sender_account_holder"  >
														{{$depositedit->sender_account_holder}}
														@foreach($bankholder as $row)
														<option value="{{$row->id}}"

															<?php if ($row->id == $depositedit->sender_account_holder) {
															echo "selected";
														} ?>>{{$row->bank_holder_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Account Number</label>
													<input type="text" name="sender_account_no" readonly="" id="sender_account_no" value="{{$depositedit->sender_account_no}}" class="form-control">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Address</label>
													<input type="text" name="sender_account_address" readonly="" id="sender_account_address" class="form-control" value="{{$depositedit->sender_account_address}}">
												</div>
											</div>
											
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Pay Mode</label>
													<select class="form-control" id="name" name="pay_mode">
														
														<option value="bank" name="bank" <?php if ($depositedit->pay_mode == 'bank') {
															echo "selected";
														}  ?> >Bank</option>
														<option value="cash" <?php if ($depositedit->pay_mode == 'cash') {
															echo "selected";
														}  ?>>Cash</option>
														<option value="bonus" <?php if ($depositedit->pay_mode == 'bonus') {
															echo "selected";
														}  ?>>Bonus</option>
													</select>
												</div>
											</div>
										</div>
										
										<div class="form-row bank box">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<span style="float: right; border: 1px solid grey; padding: 2px"> Balance: <span id="senderbalance" > </span> </span>
													<label> Send Bank </label>
													<select class="form-control"  name="recive_bank" id="recive_bank">
														<option selected disabled="">Select</option>
														@foreach($banks as $bank)
														<option value="{{$bank->id}}"

															<?php if ($bank->id == $depositedit->recive_bank) {
															echo "selected";
														} ?>>{{$bank->bank_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Account Holder</label>
													<select class="form-control " name="recive_holder_name"  id="recive_holder_name"  >
														@foreach($bankholder as $row)
														<option value="{{$row->id}}"

															<?php if ($row->id == $depositedit->recive_holder_name) {
															echo "selected";
														} ?>>{{$row->bank_holder_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Check No / Transaction</label>
													<input type="text" class="form-control " value="{{$depositedit->check_no}}"  name="check_no" value=""  >
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Payment Amount</label>
													<input type="text" name="payment_amount" value="{{$depositedit->payment_amount}}"   class="form-control">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Remarks</label>
													<input type="text" name="remarks" value="{{$depositedit->remarks}}"  class="form-control">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												
											</div>
											
											
											
										</div>
										
										
										
										<span style="float: right; border: 1px solid grey; padding: 10px">Instant Balance: <span id="instant" > </span> </span>
										<button type="submit" class="btn btn-primary">Update Deposit to Bank</button>
									</form>
								</div>
							</div>
							
							
						</div>
						
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
				</section>
			</div>
		</div>
		<script>
			
		$(document).ready(function(){
		$("#name").change(function(){
		$(this).find("option:selected").each(function(){
		var optionValue = $(this).attr("value");
		if(optionValue){
		$(".box").not("." + optionValue).hide();
		$("." + optionValue).show();
		} else{
		$(".box").hide();
		}
		});
		}).change();
		});
		</script>
		<script type="text/javascript">
		
		$(document).ready(function() {
		$('select[name="sender_bank"]').on('change', function(){
		var bankid = $(this).val();
		if(bankid) {
			$.ajax({
		url: "{{  url('/get/holder/') }}/"+bankid,
		type:"GET",
		dataType:"json",
		success:function(data) {
		
		$('#sender_account_holder').empty();
		$('#sender_account_holder').append(' <option value="">--Select--</option>');
		$.each(data,function(index,data){
		$('#sender_account_holder').append('<option value="' + data.id + '">'+data.bank_holder_name+'</option>');
		});
		},
		
		});
		} else {
		alert('danger');
		}
		});
		// subdistrict
		$('select[name="sender_account_holder"]').on('change', function(){
		var hold_id = $(this).val();
		if(hold_id) {
		$.ajax({
		url: "{{  url('/get/details/holder/') }}/"+hold_id,
		type:"GET",
		dataType:"json",
		success:function(data) {
		
		// var d =$('.rc').empty();
		$('#sender_account_no').empty();
		$('#sender_account_address').empty();
		$('#instant').empty();
		// $('.rc').append( data.name);
		// $('#rate').append( data.sell_rate);
		$('#sender_account_no').val( data.account_no);
		$('#sender_account_address').val( data.address);
		$('#instant').append( data.balance);
		}
		});
		} else {
		alert('danger');
		}
		});
		//fgfdg
		$('select[name="recive_bank"]').on('change', function(){
		var paybank_id = $(this).val();
		if(paybank_id) {
			$.ajax({
		url: "{{  url('/get/pay/bank') }}/"+paybank_id,
		type:"GET",
		dataType:"json",
		success:function(data) {
			
				$('#recive_holder_name').empty();
		$('#recive_holder_name').append(' <option value="">--Select--</option>');
		$.each(data,function(index,data){
		$('#recive_holder_name').append('<option value="' + data.id + '">'+data.bank_holder_name+'</option>');
		});
		},
		
		});
		} else {
		alert('danger');
		}
		});
		$('select[name="recive_holder_name"]').on('change', function(){
		var payment_id = $(this).val();
		if(payment_id) {
		$.ajax({
		url: "{{  url('/get/payment/bank/') }}/"+payment_id,
		type:"GET",
		dataType:"json",
		success:function(data) {
		
		$('#senderbalance').empty();
		$('#senderbalance').append( data.balance);
		}
		});
		} else {
		alert('danger');
		}
		});
		});
		
		
		</script>
		
		@endsection
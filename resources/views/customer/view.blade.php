@extends('layouts.app')
@section('content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Customer</span>
          </div>
        </div>
        <div class="panel_body">        
            
            <div class="table-responsive">
              <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
                <thead>
                  <tr>
                    <th>Customer ID</th>
                    <th>Customer Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>Bank Name</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($customers as $cust)
                  <tr>
                    <td >{{$cust->customer_id}}</td>
                    <td>{{$cust->cust_name}}</td>
                    <td>{{$cust->email}}</td>
                    <td>{{$cust->address}}</td>
                    <td>{{$cust->mobile}}</td>
                    <td>{{$cust->bank_name}}</td>
                    <td><img id="logo" src="{{asset('public/panel/customer/'.$cust->cust_image) }}" width="80" height="80px;" /></td>
                    <td>
                      <div class="btn-group" role="group">
                        <a href="{{url('single/view/'.$cust->id)}}"  class="btn btn-sm btn-secondary">View</a>
                        <a href="{{url('delete/customer')}}/{{$cust->id}}" id="delete"  class="btn btn-sm btn-danger">Delete</a>
                        <a href="{{url('edit/customer/'.$cust->id)}}" class="btn btn-sm btn-primary">Edit</a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div> <!--/ panel body -->
            </div><!--/ panel -->
           
          </section>
          <!--/ page content -->
          <!-- start code here... -->
          </div><!--/middle content wrapper-->
          </div><!--/ content wrapper -->
          @endsection
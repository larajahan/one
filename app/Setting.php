<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable=['working_day','vat','email','phone_one','phone_two','company_name','image','address','zipcode'];
}

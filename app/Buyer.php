<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{

    protected $fillable=['product_code','date','supplier_name','supplierid','address','mobile','category_id','com_mobile','item_code','product_name','rate','mrp','unit_id','qty','amount','disc','tax','company'];
    

  public function supp()
  {
    return $this->belongsTo(Supplier::class);
  }

   public function product()
  {
    return $this->belongsTo(Product::class);
  }

   public function unit()
  {
    return $this->belongsTo(Unit::class);
  }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable=['name','email','address','mobile','image','fax','contact_person','mobile_company','bank_name','account_name','account_number','branch','opening_balance',];

}

<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Image;
use File;  

class CustomerController extends Controller
{
     public function __construct()
   {
     return $this->middleware('auth');
   }


 public function customeradd()
    {
       return view('customer.add');
         
    }
    public function customerinsert(Request $request )
    {
        $request->validate([
        'cust_name' => 'required|max:30',
        'email' => 'required|email',
        'address' => 'required',
        'cust_image' => 'required|mimes:jpeg,png,jpg,bmp',
        'mobile' => 'required|numeric',
        'open_blance' => 'required',
        'bank_name' => 'required',
        'account_no' => 'required',
        'account_name' => 'required',
        'brance_name' => 'required',     
         ]); 

       $data=array();
       $data['user_id']=Auth::id();
       $data['cust_name']=$request->cust_name;
       $data['email']=$request->email;
       $data['address']=$request->address;
       $data['mobile']=$request->mobile;
       $data['open_blance']=$request->open_blance;
       $data['bank_name']=$request->bank_name;
       $data['account_no']=$request->account_no;
       $data['account_name']=$request->account_name;
       $data['brance_name']=$request->brance_name;
       $image=$request->file('cust_image');
       if ($image) {
            $image_name= hexdec(uniqid());           
            $filename=$image_name.".".$image->getClientOriginalExtension();
            Image::make($image)->resize(200, 200)->save('public/panel/customer/'.$filename);
            $data['cust_image']=$filename;

            $custinsert=DB::table('customers')
                        ->insertGetId($data);
            $day=date('d');
            $month=date('Y');
            $customer_id='C-'.$custinsert.$day.$month; 
            $customer=DB::table('customers')->where('id',$custinsert)->update(['customer_id' => $customer_id]);
             $notification=array(
                 'messege'=>'Successfully Customer Inserted ',
                 'alert-type'=>'success'
                );
            return Redirect()->route('list.customer')->with($notification);                  
       }else{

           $custinsert=DB::table('customers')
                    ->insertGetId($data);
            $day=date('d');
            $month=date('Y');
            $customer_id='C-'.$custinsert.$day.$month; 
            $customer=DB::table('customers')->where('id',$custinsert)->update(['customer_id' => $customer_id]);
             $notification=array(
                 'messege'=>'Successfully Customer Inserted ',
                 'alert-type'=>'success'
                );
            return Redirect()->route('list.customer')->with($notification);        

       }
      
  }

    public function customerview()
    {

         $customers=Customer::all()->where('user_id',Auth::id());
        return view('customer.view',compact('customers'));   
    }
   
   //Delete
    public function customerdelete($cust_id)   {
      
          $post=DB::table('customers')->where('id',$cust_id)->first();
          $image=$post->cust_image;
          $delete=DB::table('customers')->where('id',$cust_id)->delete();

           if ($delete) {
              unlink($image);
          $notification=array(
            'messege'=>'Employee Delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->to('/list/customer')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }

    }
    //Update

    public function customeredit($cust_id)
    {
        $custedit= DB::table('customers')->where('id',$cust_id)->first();

           return view('customer.edit',compact('custedit'));
    }

    public function customeredupdate(Request $request)
    {
             $old=$request->oldpic;
             $id=$request->id;

            $custedit=Customer::where('id',$id)->update([
           'cust_name' => $request->cust_name,
           'email' => $request->email,          
           'address' => $request->address,
           'mobile' => $request->mobile, 
           'bank_name' => $request->bank_name,
           'account_no' => $request->account_no,           
           'brance_name' => $request->brance_name,
           'account_name' => $request->brance_name,
           'open_blance' => $request->open_blance,
           
            ]);

            if($request->hasFile('cust_image')) 
            {
            unlink('public/panel/customer/'.$old);
            $photo=$request->cust_image; 
            $filename=hexdec(uniqid()).".".$photo->getClientOriginalExtension();
            Image::make($photo)->resize(200, 200)->save('public/panel/customer/'.$filename);
              
             Customer::find($id)->update([
                'cust_image'=> $filename               
              ]);
              
                $notification=array(
               'messege'=>'Customer Updated Successfully',
               'alert-type'=>'success'
                  );
               return Redirect()->back()->with($notification);
               
            
             }
    }

    public function custsingleview($cust_id)
             {
              $single_view=Customer::where('id',$cust_id)->first();
               return view('customer.singleview',compact('single_view'));

             }



}





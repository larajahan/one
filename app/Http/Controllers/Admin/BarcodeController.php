<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Barcode;
use App\Product;
use \Milon\Barcode\DNS1D;
use PDF;
class BarcodeController extends Controller
{
    

 public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function index()
    {
  $products=DB::table('products')->get();
  return view('admin.barcode.index',compact('products'));
 }
   public function create()
   {
    return view('admin.barcode.create');
   }

   public function store(Request $request)
  {
      $request->validate([
            'barcode' => 'required|max:30',
            
         ]); 
          $data=array();
          $data['barcode']=$request->barcode;
          $barcode=DB::table('barcodes')
                        ->insert($data);

                if ($barcode) {           
          $notification=array(
            'messege'=>'barcode Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.barcodes')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }

         

  }

 public function edit($id)
 {
      $barcode=DB::table('units')->where('id',$id)->first();
      return view('admin.barcode.edit',compact('barcode'));
 }
  public function update(Request $request,$id)
  {
      $validatedData = $request->validate([
        'name' => 'required|max:25',
        
       ]);

        $data=array();
        $data['name'] =$request->name;
        $unity=DB::table('units')->where('id',$id)->update($data);
          if ($unity) {           
          $notification=array(
            'messege'=>'unity Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.barcodes')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
       
    }


     public function delete($id)
    {
      $delete=DB::table('barcodes')->where('id',$id)->delete();
     if ($delete) {           
          $notification=array(
            'messege'=>'Barcode delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
        
    }

    public function Barcode($id)
    {
    	 $product=DB::table('products')->where('id',$id)->first();
    	 return view('admin.barcode.create',compact('product'));
    }

    public function Barcodestore(Request $request)
    {
    	   
    	   $code=$request->product_code;
    	   $name=$request->product_name;
    	   $price=$request->sell_price;
    		$d = new DNS1D();
			$d->setStorPath(__DIR__."/cache/");
			echo $d->getBarcodeHTML($code, "ISSN");
    }

   public function view(request $request)
   {
      $product_id=$request->product_id;
      $product=DB::table('products')->where('id',$product_id)->first();
      $howmany=$request->howmany;

      return view('admin.barcode.view',compact('product','howmany'));
   }

   public function General($id)
   {
    $product = Product::find($id);
    
    //return view('backend.pages.orders.invoice', compact('order'));

    $pdf = PDF::loadView('admin.barcode.invoice', compact('product'));
    
    return $pdf->stream('invoice.pdf');
    //return $pdf->download('invoice.pdf');
   }
}

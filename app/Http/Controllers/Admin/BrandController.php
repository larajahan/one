<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use App\Brand;

class BrandController extends Controller
{
     public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function index()
    {
  $brands=DB::table('brands')->get();
  return view('admin.brand.index',compact('brands'));
 }
   public function create()
   {
    return view('admin.brand.create');
   }

   public function store(Request $request)
  {
      $request->validate([
            'name' => 'required|max:30',
            
         ]); 
          $data=array();
          $data['name']=$request->name;
          $brand=DB::table('brands')
                        ->insert($data);

                if ($brand) {           
          $notification=array(
            'messege'=>'brand Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }

         

  }

 public function edit($id)
 {
      $brand=DB::table('brands')->where('id',$id)->first();
      return view('admin.brand.edit',compact('brand'));
 }
  public function update(Request $request,$id)
  {
      $validatedData = $request->validate([
        'name' => 'required|max:25',
        
       ]);

        $data=array();
        $data['name'] =$request->name;
        $brand=DB::table('brands')->where('id',$id)->update($data);
          if ($brand) {           
          $notification=array(
            'messege'=>'brand Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.brands')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
       
    }


     public function delete($id)
    {
      $delete=DB::table('brands')->where('id',$id)->delete();
     if ($delete) {           
          $notification=array(
            'messege'=>'category delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
        
    }
}

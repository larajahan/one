<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Brand;
class UnitController extends Controller
{
      public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function index()
    {
  $units=DB::table('units')->get();
  return view('admin.unity.index',compact('units'));
 }
   public function create()
   {
    return view('admin.unity.create');
   }

   public function store(Request $request)
  {
      $request->validate([
            'name' => 'required|max:30',
            
         ]); 
          $data=array();
          $data['name']=$request->name;
          $brand=DB::table('units')
                        ->insert($data);

                if ($brand) {           
          $notification=array(
            'messege'=>'brand Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }

         

  }

 public function edit($id)
 {
      $unity=DB::table('units')->where('id',$id)->first();
      return view('admin.unity.edit',compact('unity'));
 }
  public function update(Request $request,$id)
  {
      $validatedData = $request->validate([
        'name' => 'required|max:25',
        
       ]);

        $data=array();
        $data['name'] =$request->name;
        $unity=DB::table('units')->where('id',$id)->update($data);
          if ($unity) {           
          $notification=array(
            'messege'=>'unity Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.unities')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
       
    }


     public function delete($id)
    {
      $delete=DB::table('units')->where('id',$id)->delete();
     if ($delete) {           
          $notification=array(
            'messege'=>'unity delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
        
    }
}

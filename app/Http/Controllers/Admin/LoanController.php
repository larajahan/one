<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Loan;
use App\Loanfrom;
use DB;

class LoanController extends Controller
{    


	  public function __construct()
    {
        $this->middleware('auth:admin');
    }

   
      public function insertloanfrom(Request $request)
    {     
    	  $data=array();               
          $data['loanfrom']=$request->loanfrom;
          DB::table('loanfroms')
                       ->insert($data); 
                       return back(); 
    } 

     public function deleteloanfrom($loanfrom_id)
       {
        $delete=DB::table('loanfroms')->where('id',$loanfrom_id)->delete();
       
         if ($delete) {           
             $notification=array(
            'messege'=>'Loanfrom Delete Successfully',
            'alert-type'=>'success'
             );
              return Redirect()->back()->with($notification);
            }
         else{
             $notification=array(
             'messege'=>'Failed!',
             'alert-type'=>'error'
              );
           return Redirect()->back()->with($notification);
        }
    }

        public function editloanfrom($loanfrom_id)
    {    

    	 $loanfroms=DB::table('loanfroms')->where('id',$loanfrom_id)->first();
           // return response()->json($loanfroms);
    	 return view('admin.loan.editloanfrom',compact('loanfroms'));
    }  
       public function updateloanfrom (Request $request)
    {  

    	 $id=$request->id;
    	  $data=array();               
          $data['loanfrom']=$request->loanfrom;
         $loanfromupdate= DB::table('loanfroms')->where('id',$id)
                       ->update($data); 
           if ($loanfromupdate) {           
          $notification=array(
            'messege'=>'Loan From Updated Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->to('admin/add/loan/bank')->with($notification);
            }
          else{
            $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
            return Redirect()->back()->with($notification);
           }
    } 
     

  public function addloan (Request $request)

    {
        $loanfroms=DB::table('loanfroms')->get();
    	 return view('admin.loan.addloan',compact('loanfroms'));
      } 

     public function insertloan(Request $request)
    {
      $data=array(); 
          $data['date']=$request->date;
          $data['loan_from']=$request->loan_from;
          $data['title']=$request->title;
          $data['amount']=$request->amount;
          $data['interest']=$request->interest;   
          $data['payable']=$request->payable;
          $data['remarks']=$request->remarks;
          $loans=DB::table('loans')
                        ->insert($data);	
    }
}

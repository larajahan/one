<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;
use App\Buyer;
use DB;
class BuyerController extends Controller
{
    
 public function __construct()
  {
    $this->middleware('auth:admin');
  }
   

 public function index()
    {
      $buyers = DB::table('buyers')
            ->join('products', 'buyers.product_name', '=', 'products.id')
            ->join('units', 'buyers.unit_id', '=', 'units.id')
            //->join('suppliers', 'buyers.supplier_name', '=', 'suppliers.id')
            ->select('buyers.*', 'products.product_name','units.name')
            ->get();
     return view('admin.buyer.index',compact('buyers'));
    }

     public function create()
     {
      return view('admin.buyer.create');
      }

      public function SupplierGet($supplierid)
      {
      $supp=DB::table('suppliers')->where('id',$supplierid)->select('id','supplier_id','mobile','address')->first();
    	return json_encode($supp);
      }

      public function productGet($productid)
      {
      $pro=DB::table('products')->where('id',$productid)->select('id','product_code','buy_price')->first();
      return json_encode($pro);
      }


      public function store(Request $request)
      {

         $request->validate([
            'invoice' => 'required|max:30',
            'date' => 'required',
            'supplier_name' => 'required',
            'supplierid' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'category_id' => 'required',
            'item_code' => 'required',
            'product_name' => 'required',
            'rate' => 'required',
            'mrp' => 'required',
            'unit_id' => 'required',
            'qty' => 'required',
            'amount' => 'required',
            'disc' => 'nullable',
            'tax' => 'nullable',
            'company' => 'required',
            
         ]); 
          $data=array();
          $data['invoice']=$request->invoice;
          $data['date']=$request->date;
          $data['supplier_name']=$request->supplier_name;
          $data['supplierid']=$request->supplierid;
          $data['address']=$request->address;
          $data['mobile']=$request->mobile;
          $data['category_id']=$request->category_id;
          $data['company']=$request->company;
          $data['item_code']=$request->item_code;
          $data['product_name']=$request->product_name;
          $data['rate']=$request->rate;
          $data['mrp']=$request->mrp;
          $data['unit_id']=$request->unit_id;
          $data['qty']=$request->qty;
          $data['amount']=$request->amount;
          $data['disc']=$request->disc;
          $data['tax']=$request->tax;
          $buyer=DB::table('buyers')
                        ->insert($data);
                if ($buyer) {           
          $notification=array(
            'messege'=>'buyer Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
        }
      }


  public function General($id)
   {
    $buyer = Buyer::find($id);
    $pdf = PDF::loadView('admin.buyer.invoice', compact('buyer'));
    return $pdf->stream('invoice.pdf');
    //return $pdf->download('invoice.pdf');
   }
   public function edit($id)
   {
      $buyer=DB::table('buyers')->where('id',$id)->first();
      return view('admin.buyer.edit',compact('buyer'));
   }   

   public function update(Request $request,$id)
  {
      $validatedData = $request->validate([
        'date' => 'required|max:25',
        'category_id' => 'required',
        'company' => 'required',
        'unit_id' => 'required',
        'disc' => 'nullable',
        'tax' => 'nullable',
        
       ]);

        $data=array();
        $data['date'] =$request->date;
        $data['category_id'] =$request->category_id;
        $data['company'] =$request->company;
        $data['unit_id'] =$request->unit_id;
        $data['disc'] =$request->disc;
        $data['tax'] =$request->tax;
        $buyer=DB::table('buyers')->where('id',$id)->update($data);
          if ($buyer) {           
          $notification=array(
            'messege'=>'buyer Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.buyers')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
       
    } 

   public function delete($id)
    {
      $delete=DB::table('buyers')->where('id',$id)->delete();
     if ($delete) {           
          $notification=array(
            'messege'=>'buyers delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
        
    }


       
                // @php
                //   $total_amount=App\Buyer::sum('amount');
                // @endphp
      
}

<?php

namespace App\Http\Controllers\Admin;
use App\Bank;
use App\Deposit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class DepositController extends Controller
{   
	  public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function adddeposit()
    {     $banks = Bank::all();    
                       
         return view('admin.deposit.addeposit',compact('banks'));
     }


     public function insertdeposit (Request $request)
     {     
          
             $request->validate([
            'sender_bank' => 'required',  
            'sender_account_holder' => 'required',            
            'pay_mode' => 'required',            
            'payment_amount' => 'required|numeric',
            
         ]);



          $data=array();
               
          $data['invoice_no']=$request->invoice_no;         
          $data['date']=$request->date;
          $data['sender_bank']=$request->sender_bank;
          $data['sender_account_holder']=$request->sender_account_holder;
          $data['sender_account_no']=$request->sender_account_no;
          $data['sender_account_address']=$request->sender_account_address;   
          $data['pay_mode']=$request->pay_mode;
          $data['recive_bank']=$request->recive_bank;
          $data['recive_holder_name']=$request->recive_holder_name;         
          $data['payment_amount']=$request->payment_amount;
          $data['check_no']=$request->check_no;
          $data['remarks']=$request->remarks;
          $deposits=DB::table('deposits')
                        ->insert($data);

                         if ($deposits) {           
            $notification=array(
              'messege'=>'Deposits Added Successfully',
              'alert-type'=>'success'
               );
             return Redirect()->to('admin/all/deposits')->with($notification);
           }
     else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
                     
     }
      

     public function viewdeposit()
       {       
        $deposits = DB::table('deposits')
            ->leftJoin('banks', 'deposits.sender_bank','banks.id')            
            ->leftJoin('banks as otherbank', 'deposits.recive_bank','otherbank.id')            
            ->leftJoin('bankholders', 'deposits.sender_account_holder','bankholders.id')            
            ->leftJoin('bankholders as reciveholders', 'deposits.recive_holder_name','reciveholders.id')            
            ->select('deposits.*', 'banks.bank_name','otherbank.bank_name as recieve_bank','bankholders.bank_holder_name as sender_holder_name','reciveholders.bank_holder_name as whorecieve')
            ->get();  


         // return response()->json($deposits);         
          return view('admin.deposit.viewdeposit',compact('deposits'));
           
        }
        

     public function deletedeposit($deposit_id)
       {
        $delete=DB::table('deposits')->where('id',$deposit_id)->delete();
       
         if ($delete) {           
             $notification=array(
            'messege'=>'Deposits Delete Successfully',
            'alert-type'=>'success'
             );
              return Redirect()->back()->with($notification);
            }
         else{
             $notification=array(
             'messege'=>'Failed!',
             'alert-type'=>'error'
              );
           return Redirect()->back()->with($notification);
        }
    }

      public function editdeposit($deposit_id)
    {    
        
              
         $depositedit=DB::table('deposits')->where('id',$deposit_id)->first();
           // return response()->json($depositedit);
               return view('admin.deposit.edit',compact('depositedit'));
    }

     public function updatedeposit(Request $request)
    {
           $id=$request->id;
          $data=array();
               
           $data['date']=$request->date;         
           $data['sender_bank']=$request->sender_bank;
           $data['sender_account_holder']=$request->sender_account_holder;
           $data['sender_account_no']=$request->sender_account_no;
           $data['sender_account_address']=$request->sender_account_address;
           $data['pay_mode']=$request->pay_mode;   
           $data['recive_bank']=$request->recive_bank;
           $data['recive_holder_name']=$request->recive_holder_name;
           $data['check_no']=$request->check_no;
           $data['payment_amount']=$request->payment_amount;
           $data['remarks']=$request->remarks;
          $updatedeposits=DB::table('deposits')->where('id',$id)
                        ->update($data);

            if ($updatedeposits
            ) {           
          $notification=array(
            'messege'=>'Deposits Updated Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->to('admin/all/deposits')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
    }

 public function depositsingleview($deposit_id)
             {

             $single_view = DB::table('deposits')
            ->leftJoin('banks', 'deposits.sender_bank','banks.id')            
            ->leftJoin('banks as otherbank', 'deposits.recive_bank','otherbank.id')            
            ->leftJoin('bankholders', 'deposits.sender_account_holder','bankholders.id')            
            ->leftJoin('bankholders as reciveholders', 'deposits.recive_holder_name','reciveholders.id')            
            ->select('deposits.*', 'banks.bank_name','otherbank.bank_name as recieve_bank','bankholders.bank_holder_name as sender_holder_name','reciveholders.bank_holder_name as whorecieve')
            ->where('deposits.id',$deposit_id)
            ->first();  
             return view('admin.deposit.singleview',compact('single_view'));            
             }


     //Ajax 

    public function GetHolder($bankid)
    {
    	 $bank=DB::table('bankholders')->where('bank_id',$bankid)->select('id','bank_holder_name')->get();
       
    	return json_encode($bank);
    }

    public function GetDetailsHolder($hold_id)
    {
         $bank=DB::table('bankholders')->where('id',$hold_id)->select('id','account_no','address','balance')->first();
       
        return json_encode($bank);
    }
      public function GetPaybank($paybank_id)
    {
         $bank=DB::table('bankholders')->where('bank_id',$paybank_id)->select('id','bank_holder_name','balance')->get();
       
        return json_encode($bank);
    }  
    public function Pamentbank($payment_id)
    {
         $bank=DB::table('bankholders')->where('id',$payment_id)->select('id','balance')->first();
       
        return json_encode($bank);
    }


     


    
       

}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use App\Category;
  
class CategoryController extends Controller
{
    public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function index()
    {
  $categories=DB::table('categories')->get();
  return view('admin.category.index',compact('categories'));
 }
   public function create()
   {
    return view('admin.category.create');
   }

   public function store(Request $request)
  {
      $request->validate([
            'name' => 'required|max:30',
            
         ]); 
          $data=array();
          $data['name']=$request->name;
          $category=DB::table('categories')
                        ->insert($data);

                if ($category) {           
          $notification=array(
            'messege'=>'category Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }

         

  }

 public function edit($id)
 {
      $category=DB::table('categories')->where('id',$id)->first();
      return view('admin.category.edit',compact('category'));
 }
  public function update(Request $request,$id)
  {
      $validatedData = $request->validate([
        'name' => 'required|max:25',
        
       ]);

        $data=array();
        $data['name'] =$request->name;
        $category=DB::table('categories')->where('id',$id)->update($data);
          if ($category) {           
          $notification=array(
            'messege'=>'category Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.category.index')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
       
    }


     public function delete($id)
    {
      $delete=DB::table('categories')->where('id',$id)->delete();
     if ($delete) {           
          $notification=array(
            'messege'=>'category delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
        
    }


     

}


<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use DB;
use Image;
use File;
use App\Setting;
class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

   public function edit()
    {

       $settings=DB::table('settings')->first();
      return view('admin.setting.edit',compact('settings'));
    }

   public function update(Request $request)
    {
             $old=$request->oldpic;
             $id=$request->id;

            

            if($request->hasFile('image')) 
            {
            unlink('public/panel/setting/'.$old);
            $photo=$request->image; 
            $filename=hexdec(uniqid()).".".$photo->getClientOriginalExtension();
            Image::make($photo)->resize(200, 200)->save('public/panel/setting/'.$filename);
              
             Setting::where('id',$id)->update([
                'image'=> $filename               
              ]);
              
                $notification=array(
               'messege'=>'Setting Updated Successfully',
               'alert-type'=>'success'
                  );
               return Redirect()->back()->with($notification);
             }
             $settings=Setting::where('id',$id)->update([
	           'working_day' => $request->working_day,
	           'vat' => $request->vat,          
	           'email' => $request->email,
	           'phone_one' => $request->phone_one, 
	           'phone_two' => $request->phone_two, 
	           'company_name' => $request->company_name, 
	           'address' => $request->address, 
	           'zipcode' => $request->zipcode, 
            ]);
              $notification=array(
               'messege'=>'Setting Updated Successfully',
               'alert-type'=>'success'
                  );
               return Redirect()->back()->with($notification);

    }

}

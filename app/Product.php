<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

  protected $primaryKey = 'id';

  protected $fillable=[
  'product_code','product_name','category_id','brand_id','unit_id','buy_price','sell_price','vat','low_stock','up_stock','status','image','stock'
  ];

  

  public function brand()
  {
    return $this->belongsTo(Brand::class);
  }

   public function unit()
  {
    return $this->belongsTo(Unit::class);
  }
    public function category()
  {
    return $this->belongsTo('App\Category', 'category_id', 'id');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
     protected $fillable = ['invoice_no','date', 'bank', 'account_no','account_holder_name','pay_mode','recive_bank','recive_holder_name','check_no','payment_amount','remarks'];
}

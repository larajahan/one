<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraws', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice_no')->nullable();            
             $table->string('date')->nullable();
             $table->string('bank')->nullable();
             $table->string('account_no')->nullable();
              $table->string('account_holder_name')->nullable();         
             $table->string('pay_mode')->nullable();
              $table->string('recive_bank')->nullable();
             $table->string('recive_holder_name')->nullable();
             $table->string('check_no')->nullable();  
             $table->string('payment_amount')->nullable();           
             $table->string('remarks')->nullable();        
         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraws');
    }
}

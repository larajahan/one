<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_code')->nullable();
            $table->string('date')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('supplierid')->nullable();
            $table->string('address')->nullable();
            $table->string('mobile')->nullable();
            $table->string('category_id')->nullable();
            $table->string('com_mobile')->nullable();
            $table->string('item_code')->nullable();
            $table->string('product_name')->nullable();
            $table->string('rate')->nullable();
            $table->string('mrp')->nullable();
            $table->string('unit_id')->nullable();
            $table->string('qty')->nullable();
            $table->string('amount')->nullable();
            $table->string('disc')->nullable();
            $table->string('tax')->nullable();
            $table->string('company')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyers');
    }
}
